package io.gitlab.porygonzrocks.kotorpatcher;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.concurrent.ExecutionException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.support.graphics.drawable.VectorDrawableCompat;

import com.ipaulpro.afilechooser.utils.FileUtils;
import com.ipaulpro.afilechooser.FileChooserActivity;
import com.github.rjeschke.txtmark.Processor;
import org.ini4j.Wini;
import org.ini4j.Profile.Section;
import pkg2da.editor.TwoDABroker;

import io.gitlab.porygonzrocks.misc.Utils;


public class MainActivity extends AppCompatActivity {
	private int version;
	private final int REQUEST_CODE = 3995; 																								// Random
	private String TAG = "KOTORPatcher";																								// TAG log stuff as KOTORPatcher
	int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = Utils.getRandomNumber(); 														// "Completely random" (https://www.xkcd.com/221/)
	final Activity thisActivity = this;																									// This activity
	private TextView textview = null;																									// The main TextView
	AlertDialog.Builder popupBuilder = null;
	AlertDialog popup = null;
	@Override																															// When the avtivity is made, needs default stuff
	protected void onCreate(Bundle savedInstanceState) {																				// When the activity is created
		super.onCreate(savedInstanceState);																								// Something put here by default?
		setContentView(R.layout.main);																									// Set the layout to main
		Toolbar appbar = (Toolbar) findViewById(R.id.appbar);
		setSupportActionBar(appbar);
		popupBuilder = new AlertDialog.Builder(this);
		popupBuilder.setView(R.layout.popup);
		popup = popupBuilder.create();
		textview = (TextView) findViewById(R.id.textview);
		try {
			version = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
		}
		catch (PackageManager.NameNotFoundException e) {
			Log.wtf(TAG, "This app is not found?", e);																					// Should not EVER happen
		}
		textview.setMovementMethod(LinkMovementMethod.getInstance());																	// Let it scroll, let it scroll, can't hold it still anymore
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			// Copied from developer.android.com, I don't need to add to the documentation
			// Here, thisActivity is the current activity
			if (getApplicationContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
				!= PackageManager.PERMISSION_GRANTED) {

				// Should we show an explanation? 
				// A: YES
//			if (thisActivity.shouldShowRequestPermissionRationale(thisActivity,
//																	WRITE_EXTERNAL_STORAGE)) {

				// Show an explanation to the user *asynchronously* -- don't block
				// this thread waiting for the user's response! After the user
				// sees the explanation, try again to request the permission.

				textview.setText("This app needs permission to access files to run!");													// Alert the user that the permission is needed
				textview.append("\nIt can't access mod folders and KOTOR files otherwise!");											// Explain why
				Button button = (Button) findViewById(R.id.button_choose_file);															// Requests permissions again
				button.setText(R.string.request_perm);																					// Properly set button text
				button.setOnClickListener(new View.OnClickListener() {																	// Set the OnClickListener
						@Override																										// Override stuff
						public void onClick(View v) {																					// When the button is clicked
							int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = Utils.getRandomNumber(); 								// "Completely random" (https://www.xkcd.com/221/)
							thisActivity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
															MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
						}
					});
//			}
//			else {

				// No explanation needed, we can request the permission.

				thisActivity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
												MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

				// MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE is an
				// app-defined int constant. The callback method gets the
				// result of the request.
//			}
			}
			else {
				prep();
			}
		}
		else {
			prep();
		}
	}
	@Override																															// Override whataver is Android default
	public boolean onCreateOptionsMenu(Menu menu) {																						// When the menu is (opened? created?)
		MenuInflater inflater = getMenuInflater();																						// Ready air pump...
		inflater.inflate(R.menu.appbar, menu);																							// BLOW IT UP! (aka inflate menu)
		MenuItem menuButton = menu.findItem(R.id.action_item_showmenu);
		menuButton.setIcon(VectorDrawableCompat.create(getResources(), R.drawable.ic_menu, null));
		menuButton.getIcon().mutate().setTint(getColor(R.color.kotor_colors));
		return super.onCreateOptionsMenu(menu);																							// Return... something?
	}
	
	@Override																															// Override that nasty blank default
	public boolean onOptionsItemSelected(MenuItem item) {																				// When a button feels pressure
		super.onOptionsItemSelected(item);																								// Do something?
		switch (item.getItemId()) {																										// Hopefully you pressed the right button...

			case R.id.action_item_showmenu:
				popup.show();
				break;

		}
		return true;																													// I guess a success message?
	}
	
	public void openSupport(View v) {
		popup.dismiss();
		Utils.openURL("http://deadlystream.com/forum/topic/5842-wip-kotorpatcher-tslpatcher-like-app-for-android/", MainActivity.this);	// User used HELP!
	}
	
	public void openUpdate(View v) {
		popup.dismiss();
		Updater.check(MainActivity.this);
	}
	
	public void openSource(View v) {
		popup.dismiss();
		Utils.openURL("https://gitlab.com/PorygonZRocks/kotorpatcher.git", MainActivity.this);											// Open link to source code
	}
	
	public void openLicenses(View v) {
		popup.dismiss();
		Intent mdIntent = new Intent(MainActivity.this, MarkdownViewer.class);
		mdIntent.putExtra("io.gitlab.porygonzrocks.kotorpatcher.Data", Utils.convertStreamToString(getResources().openRawResource(R.raw.license)));
		startActivity(mdIntent);
	}
	
	public void openTests(View v) {
		popup.dismiss();
		setContentView(R.layout.tests);																									// Switch to tests layout
		Button twoDACreator = (Button) findViewById(R.id.button_custom_twoda);
		twoDACreator.setOnClickListener(new View.OnClickListener() {																	// Set onClickListener for creator button
				public void onClick(View v) {																							// Set as onClick
					setContentView(R.layout.twodamaker);
					Button makeTwoDA = (Button) findViewById(R.id.button_make_twoda);
					makeTwoDA.setOnClickListener(new View.OnClickListener() {															// Set onClickListener for create button
							public void onClick(View v) {																				// Set as onClick
								String header = ((EditText) findViewById(R.id.input_twoda_header)).getText().toString();
								List<String> colNames = Arrays.asList(((EditText) findViewById(R.id.input_twoda_colnames))
																	  .getText().toString().split(","));
								List<String> rowNames = Arrays.asList(((EditText) findViewById(R.id.input_twoda_rownames))
																	  .getText().toString().split(","));
								int rowCount = rowNames.size();
								int colCount = colNames.size();
								String[] dataInRows = (((EditText) findViewById(R.id.input_twoda_data))
									.getText().toString().split("\\n"));
								Object[][] data = new Object[rowCount][colCount];
								for (int i = 0; i < dataInRows.length; i++) {
									data[i] = dataInRows[i].split(",");
								}
								File path = new File(((EditText) findViewById(R.id.input_twoda_path)).getText().toString());
								pkg2da.editor.TwoDA twoDA = new pkg2da.editor.TwoDA(header, 
																					colNames, 
																					rowNames, 
																					rowCount, 
																					colCount, 
																					data);
								TwoDA newTwoDA = new TwoDA(twoDA, path);
								newTwoDA.write();
								TextView twoDAView = (TextView) findViewById(R.id.textview);
								twoDAView.setText("2DA made!");
							}
						});
				}
			});}

	private void patch(String path, boolean nmspcs) {																					// Requires an ini, and whether to have namespaces or no
		new Patcher().execute(new String[]{path,Boolean.toString(nmspcs)});
	}

	private void prep() {
		textview.setText(R.string.output_here);
		Button button = (Button) findViewById(R.id.button_choose_file);																	// Starts aFileChooser (I should rename it...)
		button.setText(R.string.choose_file);																							// Make sure title is correct!
		button.setOnClickListener(new View.OnClickListener() {																			// Set the OnClickListener
				@Override																												// Override stuff
				public void onClick(View v) {																							// When the file chooser button is clicked, choose a file
					Intent chooserIntent = FileUtils.createGetContentIntent();
					chooserIntent.setType("application/vnd.microsoft.portable-executable");
					chooserIntent.setClass(MainActivity.this, FileChooserActivity.class);
					chooserIntent.addCategory(Intent.CATEGORY_OPENABLE);
					startActivityForResult(chooserIntent, REQUEST_CODE);
				}
			});
		Button readINIButton = (Button) findViewById(R.id.button_install);																// The install button
		readINIButton.setVisibility(View.VISIBLE);																						// Unhide
		final EditText patcherPathInput = (EditText) findViewById(R.id.input_patcher_path);												// The input (usually auto filled by aFileManager)
		patcherPathInput.setVisibility(View.VISIBLE);																					// Make visible
		readINIButton.setOnClickListener(new View.OnClickListener() {																	// Set the OnClickListener
				public void onClick(View v) {																							// When you click THE BUTTON
					File patcherFile = new File(patcherPathInput.getText().toString());													// TSLPatcher.exe
					String tslpatchdata = patcherFile.getParent() + "/tslpatchdata";													// tslpatchdata folder
					//String patcherRoot = new File(tslpatchdata).getParent();															// TSLPatcher.exe folder
					String changesINI = tslpatchdata + "/changes.ini";																	// changes.ini
					String nmspcINI = tslpatchdata + "/namespaces.ini";																	// namespaces.ini
					boolean nmspcs = false;
					if (new File(nmspcINI).exists()) {																					// Is there a namespaces.ini?
						nmspcs = true;
					}
					patch(changesINI, nmspcs);																							// Install mod, pass existence of namespaces.ini
				}
			});
		File oldUpdateApk = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()
									 + "/kotorpatcher.apk");																			// The path to any theoretical old update file
		if (oldUpdateApk.exists()) {																									// Is there an old update file?
			if (!oldUpdateApk.delete()) {																								// Delete it
				oldUpdateApk.deleteOnExit();																							// Try again, maybe it'll work?
			}
		}
	}

	@Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
										   int[] grantResults) {
		if (requestCode == MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE
			&& getApplicationContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
			== PackageManager.PERMISSION_GRANTED) {
			prep();
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case REQUEST_CODE:
				// If the file selection was successful
				if (resultCode == RESULT_OK) {
					if (data != null) {
						// Get the URI of the selected file
						final Uri uri = data.getData();
						Log.i(TAG, "Uri = " + uri.toString());
						try {
							// Get the file path from the URI
							final String path = FileUtils.getPath(this, uri);
							EditText path_input = (EditText) findViewById(R.id.input_patcher_path);
							path_input.setText(path);
						}
						catch (Exception e) {
							Log.e("FileSelectorTestActivity", "File select error", e);
						}
					}
				}
				break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private class Patcher extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String[] p1) {
			String path = p1[0];
			boolean nmspcs = Boolean.valueOf(p1[1]);
			String modsDir = "/storage/emulated/0/Android/data/com.aspyr.swkotor/files/";													// Usual mod directory
			if (!new File(modsDir).exists()) {																								// If normal mod dir does not exist
				if (new File("/storage/emulated/0/Android/data/com.aspyr.swkotor.underground/files").exists()) {							// If Underground exists
					modsDir = "/storage/emulated/0/Android/data/com.aspyr.swkotor.underground/files/";										// KOTOR Underground mod directory
				}
				else {
					modsDir = "Nonexistent";																								// None exist
					publishProgress("Please install KOTOR!", "true");																		// Display message explaining issue
				}
			}
			else {
				int i = 0;																													// Initialize i
				SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss:SS");															// Format for Utils.timeStamps
				dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));																		// Use GMT (I had a reason? Uhhh...)
				Wini ini = new Wini();																										// Initialize ini object
				try {																														// <--START TRY-->
					ini.load(new File(path));																								// Load changes.ini
				}																															// <--END TRY-->
				catch (IOException e) {																										// <--START CATCH-->
					Log.e(TAG, "Error loading ini!", e);
				}																															// <--END CATCH-->
				String[] sections = new String[ini.size()];																					// Will list sections of ini as string
				for (String sectionName: ini.keySet()) {																					// Begin for loop
					sections[i] = sectionName;																								// Set sections to var
					i++;																													// Increase i
				}																															// End for loop
				Section[] section = new Section[ini.size()];																				// Create list of the sections
				for (i = 0; i < ini.size(); i++) {																							// Begin for loop
					section[i] = ini.get(sections[i]);																						// Add sections to array
				}																															// End for loop
				i = 0;																														// Reset i
				String[] InstallList = new String[ini.get("InstallList").size()];															// Will list folders to install to (name of ini header)
				String[] InstallDirs = new String[ini.get("InstallList").size()];															// Will list directories to install to (lowercase)
				String[][] InstallFiles = new String[InstallList.length][];																	// Will list files to install (and folders they belong to)
				for (String key: ini.get("InstallList").keySet()) {																			// Begin for loop
					InstallList[i] = key;																									// Set files in list
					InstallDirs[i] = ini.get("InstallList", InstallList[i]).toLowerCase();													// Undercase list of folders
					i++;																													// Increment i
				}																															// End for loop
				for (i = 0; i < InstallList.length; i++) {																					// <--START FOR LOOP-->
					InstallFiles[i] = new String[ini.get(InstallList[i]).size()];															// Innitialize sub arrarys
					int j = 0;																												// Initialize j (tracking int)
					for (String key: ini.get(InstallList[i]).keySet()) {																	// <--START FOR LOOP-->
						InstallFiles[i][j] = ini.get(InstallList[i], key);																	// Make list of files
						j++;																												// Increment j
					}																														// End for loop
				}																															// End for loop
				String[] TwoDAList = new String[ini.get("2DAList").size()]; 																// Used to store list of 2DA files to be patched 
				String[][] TwoDAEdits = new String[TwoDAList.length][];																		// List of edits to be made
				i = 0;																														// Reset i
				for (String key: ini.get("2DAList").keySet()) {																				// <--START FOR LOOP-->
					TwoDAList[i] = key;																										// Fill variable with lisy
					i++;																													// Increment i
				}																															// End for loop
				for (i = 0; i < TwoDAList.length; i++) {																					// <--START FOR LOOP-->
					TwoDAEdits[i] = new String[ini.get(ini.get("2DAList", TwoDAList[i])).size()];
					//TwoDADiffs[i] = ini.get(ini.get("2DAList", TwoDAList[i]));
					int j = 0;
					for (String key: ini.get(ini.get("2DAList", TwoDAList[i])).keySet()) {
						TwoDAEdits[i][j] = key.toString();
						j++;
					}
				}

				publishProgress("Preparing...", "true");
				publishProgress("\n✔️ Done, ⚠️ Warning, ️❌️ Failed, ☢️ Experimental, 🚫 Unimplemented, ❎ Unsupported by KOTOR");
				for (i = 0; i < InstallList.length; i++) {
					for (int f = 0; f < InstallFiles[i].length; f++) {
						String modDir = modsDir + InstallDirs[i];
						publishProgress("\n [" + Utils.timeStamp(dateFormat) + "] Copying " + InstallFiles[i][f] + " to " + modDir);
						if (InstallDirs[i].contains(".mod")) {
							publishProgress(" 🚫\n\tEditing *.mod files is not yet supported!");
						}
						else if (InstallDirs[i].contains("stream")) {
							publishProgress(" ❎\n\tMost custom audio does not work with Android, so to save space," + 
											"you must extract them manually if they work.");
						}
						else if (InstallDirs[i].contains("launcher")) {
							publishProgress(" ❎\n\tKOTOR on Android does not have a customizable launcher. Not extracted to save space.");
						}
						else {
							if (!new File(modDir).exists()) {
								publishProgress("\n" + modDir + " does not exist, creating it");
								boolean dirMade = new File(modDir).mkdirs();
								if (dirMade) {
									publishProgress(" ✔️");
								}
								else {
									publishProgress(" ️❌️");
								}
							}
							File OldFile = new File(new File(path).getParent() + "/" + InstallFiles[i][f]);
							File NewFile = new File(modDir + "/" + InstallFiles[i][f]);
							if (Utils.copy(OldFile, NewFile)) {
								publishProgress(" [" + Utils.timeStamp(dateFormat) + "] ✔️");
							}
							else {
								publishProgress(" [" + Utils.timeStamp(dateFormat) + "] ❌");
							}

						}
					}
				}

				publishProgress("\n[" + Utils.timeStamp(dateFormat) + "] Beginning 2DA patching... ");
				publishProgress("\n☢️ WARNING: EXPERIMENTAL AND NOT FULLY FUNCTIONAL");
				String[] TwoDAMEMORY = new String[0];																					// 2DAMEMORY, start at 0 because method expands every time
				for (i = 0; i < TwoDAList.length; i++) {
					String modDir = modsDir + "override";																				// 2DAs always go in override
					if (!new File(modDir).exists()) {																					// If the folder does not exist
						publishProgress("\n" + modDir + " does not exist, creating it");
						boolean dirMade = new File(modDir).mkdirs();																	// Make folder (and alert success or no)
						if (dirMade) {
							publishProgress(" [" + Utils.timeStamp(dateFormat) + "] ✔️");
						}
						else {
							publishProgress(" [" + Utils.timeStamp(dateFormat) + "] ❌");
						}
					}
					publishProgress("\n[" + Utils.timeStamp(dateFormat) + "] Editing " + ini.get("2DAList", TwoDAList[i]));
					File oldFile = new File(new File(path).getParent() + "/" + ini.get("2DAList", TwoDAList[i]));						// File in tslpatchdata
					File newFile = new File(modDir + "/" + ini.get("2DAList", TwoDAList[i]));											// File in override 
					if (!newFile.exists()) {																							// If file is not in override
						publishProgress("\n[" + Utils.timeStamp(dateFormat) + "] Copying " + ini.get("2DAList", TwoDAList[i]) + " to folder");
						Utils.copy(oldFile, newFile);																					// Copy 2DA to override
						publishProgress(" [" + Utils.timeStamp(dateFormat) + "] ✔️");
					}
					TwoDA twoDA = new TwoDA(new TwoDABroker().Open(newFile), newFile);													// Editable 2DA (my wrapper around peedeeboy classes)
					for (int j = 0; j < TwoDAEdits[i].length; j++) {
						String row = ini.get(ini.get("2DAList", TwoDAList[i]), TwoDAEdits[i][j]);
						if (TwoDAEdits[i][j].contains("AddRow") || TwoDAEdits[i][j].contains("CopyRow")) {								// If adding or copying
							publishProgress("\n[" + Utils.timeStamp(dateFormat) + "] Expanding array by one");
							twoDA.addRow(Integer.toString(twoDA.getRowCount()));														// Must expand for add and copy
							publishProgress(" [" + Utils.timeStamp(dateFormat) + "] ✔️");
							if (TwoDAEdits[i][j].contains("CopyRow")) {
								for (int k = 0; k < twoDA.getColumnCount(); k++) {
									String origRowIndex = ini.get(row, "RowIndex");														// Index of row to copy
									String origRow = twoDA.getDataList().get(Integer.valueOf(origRowIndex)).get(k);						// Get original value
									twoDA.getDataList().get(twoDA.getRowCount() - 1).set(k, origRow);									// Set to value from copied row
								}
							}
						}
						for (String changes: ini.get(row).keySet()) {																	// Begin for loop
							if (!changes.equals("RowIndex") && 
								!changes.equals("RowLabel") && 
								!changes.contains("2DAMEMORY") &&
								!ini.get(row, changes).contains("2DAMEMORY")) {															// Exclude RowIndex, RowLabel, and 2DAMEMORY
								int columnNum = Integer.valueOf(twoDA.getColumnNames().indexOf(changes));								// Get the column index
								twoDA.setData(twoDA.getRowCount() - 1, columnNum, ini.get(row, changes));								// Change 2DA value
							}
							else if (changes.contains("2DAMEMORY")) {																	// If it is a 2DAMEMORY slot
								TwoDAMEMORY = (String[]) Utils.expandArray(TwoDAMEMORY);														// Add 2DAMEMORY slot
								if (ini.get(row, changes).equals("RowIndex")) {															// If assigning RowIndex
									publishProgress("\n[" + Utils.timeStamp(dateFormat) + "] Assigning index "
													+ twoDA.getRowCount() + " to 2DAMEMORY slot " + TwoDAMEMORY.length);
									TwoDAMEMORY[TwoDAMEMORY.length - 1] = Integer.toString(twoDA.getRowCount());						// Assign newest row index to slot
								}
								else {
									publishProgress("\n[" + Utils.timeStamp(dateFormat) + "] Assigning "
													+ ini.get(row, changes) + " to 2DAMEMORY slot " + TwoDAMEMORY.length);
									TwoDAMEMORY[TwoDAMEMORY.length - 1] = ini.get(row, changes);										// Assign value to slot
								}
							}
							else if (ini.get(row, changes).contains("2DAMEMORY")) {														// If it needs a 2DAMEMORY slot
								int slot = Integer.valueOf(ini.get(row, changes).split("2DAMEMORY")[1]);								// Seperate slot num
								int columnNum = Integer.valueOf(twoDA.getColumnNames().indexOf(changes));								// Get the column index
								publishProgress("\n[" + Utils.timeStamp(dateFormat) + "] Retreiving "
												+ TwoDAMEMORY[slot - 1] + " from 2DAMEMORY slot " + slot
												+ " to row " + twoDA.getRowCount() + ", column " + columnNum);
								twoDA.setData(twoDA.getRowCount() - 1, columnNum, TwoDAMEMORY[slot - 1]);								// Set value to slot
							}
						}																												// End for loop
					}
					publishProgress("\n[" + Utils.timeStamp(dateFormat) + "] Saving " + ini.get("2DAList", TwoDAList[i]));		
					twoDA.write();
					publishProgress(" ✔️");
				}
			}
			return null;
		}

		protected void onProgressUpdate(String[] values) {
			super.onProgressUpdate(values);
			TextView textview = (TextView) findViewById(R.id.textview);
			if (values.length > 1) {
				if (Boolean.valueOf(values[1]) == true) {
					textview.setText(values[0]);
				}
			}
			else {
				textview.append(values[0]);
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss:SS");															// Format for Utils.timeStamps
			dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));																		// Use GMT (I had a reason? Uhhh...)
			TextView textview = (TextView) findViewById(R.id.textview);																	// Variable to refer to TextView
			String logPath = null;
			try {
				logPath = Environment.getExternalStorageDirectory().getCanonicalPath() + "/kotorpatcher/";								// Much prettier than absolute path
			}
			catch (IOException e) {
				Log.e(TAG, "Could not get canonical path!", e);
				logPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/kotorpatcher/";								// Doesn't throw IOException
			}
			if (!new File(logPath).exists()) {
				new File(logPath).mkdirs();
			}
			textview.append("\n[" + Utils.timeStamp(dateFormat) + "] Writing log to " + logPath);
			Utils.writeFile(logPath + Utils.timeStamp(dateFormat) + ".log",
							(textview.getText().toString() + " ✔️\n[" + Utils.timeStamp(dateFormat)	+ "] Done!"));
			textview.append(" ✔️\n[" + Utils.timeStamp(dateFormat)	+ "] Done!");
		}
	}
}
