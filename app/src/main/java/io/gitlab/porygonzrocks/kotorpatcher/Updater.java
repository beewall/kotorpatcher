package io.gitlab.porygonzrocks.kotorpatcher;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;

import com.github.rjeschke.txtmark.Processor;

import io.gitlab.porygonzrocks.misc.Utils;

public class Updater extends AppCompatActivity {

	public static void check(final Activity activity) {
		String TAG = "KOTORPatcher";
		Intent mdIntent = new Intent(activity, MarkdownViewer.class);
		int version = 0;
		try {
			version = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionCode;
		}
		catch (PackageManager.NameNotFoundException e) {
			Log.e(TAG, "This app is not found?", e);																					// Should not EVER happen
		}
		int latest = 0;																													// Init latest version (yep, 0 is totally most recent)
		NetworkInfo netInfo = ((ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnected()) {
			String webAss = "https://gitlab.com/PorygonZRocks/kotorpatcher/raw/master/app/src/main/res/raw/";							// It means assests, jeez
			try {
				latest = new Integer(new String(new Utils.readFromWeb().execute(webAss + "/version.txt").get()));						// Set the latest version to the latest version
			}
			catch (InterruptedException | ExecutionException | NumberFormatException e) {												// Catch a buncha problems
				Log.e(TAG, "Error loading the changelog from the web!", e);
				mdIntent.putExtra("io.gitlab.porygonzrocks.kotorpatcher.Data", Utils.convertStreamToString(activity.getResources().openRawResource(R.raw.changelog)));
			}																															// Assuming release variant
			if (latest > version) {																										// Is new version really new?
				Toast.makeText(activity.getApplicationContext(), "Update from " + version + " to " + latest, Toast.LENGTH_SHORT).show();// Toast [to] the update!
				try {
					mdIntent.putExtra("io.gitlab.porygonzrocks.kotorpatcher.Data", new String(new Utils.readFromWeb().execute(webAss + "changelog.md").get()));
				}
				catch (InterruptedException e) {}
				catch (ExecutionException e) {}
				Toast.makeText(activity.getApplicationContext(), "Downloading update", Toast.LENGTH_SHORT).show();
				mdIntent.putExtra("io.gitlab.porygonzrocks.kotorpatcher.Data", Utils.convertStreamToString(activity.getResources().openRawResource(R.raw.changelog)));
				String apkURL = "https://gitlab.com/PorygonZRocks/kotorpatcher/raw/master/release.apk";									// Download url for release apk
				BroadcastReceiver onComplete = new BroadcastReceiver() {																// Reciever to register (run on dl finish)
					public void onReceive(Context context, Intent intent) {
						String dlDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).
							getAbsolutePath();																							// Download directory
						Toast.makeText(activity.getApplicationContext(), "Installing update", Toast.LENGTH_SHORT).show();
						installApk((dlDir + "/kotorpatcher.apk"), activity);															// Pass this activity, needed for the method to be static
					}
				};
				activity.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));						// Register the intent to know when download finishes
				Utils.download(apkURL, "kotorpatcher.apk", activity);																	// Download the update
				activity.startActivity(mdIntent);
			}
			else {
				Toast.makeText(activity.getApplicationContext(), "No updates available", Toast.LENGTH_SHORT).show();					// Toast the lack of updates
			}

			activity.startActivity(mdIntent);
		}
		else {
			Toast.makeText(activity.getApplicationContext(), "No internet access!", Toast.LENGTH_SHORT).show();								// Toast the lack of updates
			mdIntent.putExtra("io.gitlab.porygonzrocks.kotorpatcher.Data", Utils.convertStreamToString(activity.getResources().openRawResource(R.raw.changelog)));
			activity.startActivity(mdIntent);
		}
	}

	private static void installApk(String apkPath, Activity activity) {
		Intent installAPKIntent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
		installAPKIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		FileProvider.getUriForFile(activity.getApplicationContext(), "io.gitlab.porygonzrocks.fileprovider", new File(apkPath));
		installAPKIntent.setData(Uri.fromFile(new File(apkPath)));
		activity.startActivity(installAPKIntent); 
	}
}
