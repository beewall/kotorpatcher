[$PROFILE$]: extended

This project is licensed under the [GNU GPL version 3](https://www.gnu.org/licenses/gpl-3.0.txt) (NOT "3 or later", due to license compatibility restrictions), excluding EditTwoDA.java, which is under the Apache 2.0, copyright by me, and anything below (see sections for details).  
However, if a later version of the GNU GPL is added to the Creative Commons Compatible Licenses, listed [here](https://creativecommons.org/share-your-work/licensing-considerations/compatible-licenses), then under GNU GPL section 14, you are free to use that later GNU GPL version.

This project uses the [\[ini4j\]](https://sourceforge.net/projects/ini4j/) library, which is licensed under the [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0.html).

In addition, this app uses [aFileChooser](https://github.com/zhuowei/aFileChooser), licensed under the Apache 2.0 by Paul Burke, as well as changes originally made by Zhuowei Zhang for Blocklauncher.  

All files in the pkg2da.editor package are made by peedeeboy on Deadlystream, used with permission. 

This app is also uses code from aFileChooserExample from that aFileChooser, which is also under the Apache 2.0 license. 

The app also uses [txtmark](https://github.com/rjeschke/txtmark) (Apache 2.0) to display the changelog (and this file!).

The permissions request method is taken directly from the developer.android.com documentation, licensed under the Apache 2.0.

The Android [appcompat-v7](https://android.googlesource.com/platform/frameworks/support.git/+/master/) library is used.

The function getRandomNumber() is taken from [xkcd 221](https://www.xkcd.com/221), licensed under the [Creative Commons BY-NC 2.5](https://creativecommons.org/licenses/by-nc/2.5/) by Randall Munroe.

KOTORPatcher uses [jarchivelib](https://github.com/thrau/jarchivelib/), licensed under the Apache 2.0, by Thomas Rausch.

The writeToFile function is based off code from [a StackOverflow answer](https://stackoverflow.com/a/27816008), by Zeeshan Ghazanfar.

The function copy (Utils) is written by Rakshi on Stackoverflow [code](https://stackoverflow.com/a/9293885).

fileToBytes in Utils is based off [another StackOverflow answer](https://stackoverflow.com/a/13353219), by RajeshVijayakumar.

The Utils function md5hash is largely based off [user49913's StackOverflow answer](https://stackoverflow.com/a/421696).

readFromURL is by StackOverflow user Martin Algesten, from [this answer](https://stackoverflow.com/a/6694210).

On StackOverflow, user2003470 created unzip [here](https://stackoverflow.com/a/10634536).

StackOverflow user pramod kumar wrote the MiscUtils download function in [this answer](https://stackoverflow.com/a/36167567).

convertStreamToString is from [SO](https://stackoverflow.com/a/5445161), by user Pavel Repin.

All code from StackOverflow answers are using the default StackOverflow license (at that time) - [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/legalcode) - unless otherwise stated.

However, for all functions under CC-BY-SA 3.0, section 4.b of that license allows me to use it under the terms of the updated [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode) instead, which is one-way compatible with the GNU GPL version 3.

The icons in aFileChooser were taken from [Google's Material Design Icons](https://github.com/google/material-design-icons), licensed under the Apache 2.0.  

  
Licenses:

    KOTORPatcher:  
          
        Copyright (C) 2017-2018 PorgonZRocks  
          
        This program is free software: you can redistribute it and/or modify  
        it under the terms of the GNU General Public License as published by  
        the Free Software Foundation, either version 3 of the License, or  
        (at your option) any later version.  
          
        This program is distributed in the hope that it will be useful,  
        but WITHOUT ANY WARRANTY; without even the implied warranty of  
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
        GNU General Public License for more details.  
          
        You should have received a copy of the GNU General Public License  
        along with this program.  If not, see <https://www.gnu.org/licenses/>.  
  
    [ini4j]:  
          
        Copyright (C) 2005-2009 Ivan SZKIBA  
          
        Licensed under the Apache License, Version 2.0 (the "License");  
        you may not use this file except in compliance with the License.  
        You may obtain a copy of the License at  
          
            <http://www.apache.org/licenses/LICENSE-2.0>  
          
        Unless required by applicable law or agreed to in writing, software  
        distributed under the License is distributed on an "AS IS" BASIS,  
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
        See the License for the specific language governing permissions and  
        limitations under the License.  

    aFileChooser:  
        
        Copyright (C) 2011-2013 Paul Burke:  
          
        Licensed under the Apache License, Version 2.0 (the "License");  
        you may not use this file except in compliance with the License.  
        You may obtain a copy of the License at  
          
            <http://www.apache.org/licenses/LICENSE-2.0>  
          
        Unless required by applicable law or agreed to in writing, software  
        distributed under the License is distributed on an "AS IS" BASIS,  
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
        See the License for the specific language governing permissions and  
        limitations under the License.  
        
    txtmark:  
        
        Copyright (C) 2011-2015 René Jeschke [René_jeschke@yahoo.de](mailto:lRené_jeschke@yahoo.de)  
          
        Licensed under the Apache License, Version 2.0 (the "License");  
        you may not use this file except in compliance with the License.  
        You may obtain a copy of the License at  
          
            <http://www.apache.org/licenses/LICENSE-2.0>  
          
        Unless required by applicable law or agreed to in writing, software  
        distributed under the License is distributed on an "AS IS" BASIS,  
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
        See the License for the specific language governing permissions and  
        limitations under the License.  
          
    Permissions request dialog:  
        
        Copyright (C) 2017 The Android Open Source Project  
          
        Licensed under the Apache License, Version 2.0 (the "License");  
        you may not use this file except in compliance with the License.  
        You may obtain a copy of the License at  
          
            <http://www.apache.org/licenses/LICENSE-2.0>  
          
        Unless required by applicable law or agreed to in writing, software  
        distributed under the License is distributed on an "AS IS" BASIS,  
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
        See the License for the specific language governing permissions and  
        limitations under the License.  
          
    appcompat-v7:  
        
        Copyright (C) 2014 The Android Open Source Project  
          
        Licensed under the Apache License, Version 2.0 (the "License");  
        you may not use this file except in compliance with the License.  
        You may obtain a copy of the License at  
          
            <http://www.apache.org/licenses/LICENSE-2.0>  
          
        Unless required by applicable law or agreed to in writing, software  
        distributed under the License is distributed on an "AS IS" BASIS,  
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
        See the License for the specific language governing permissions and  
        limitations under the License.  
          
