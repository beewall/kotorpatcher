package io.gitlab.porygonzrocks.misc;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URL;
import java.security.DigestInputStream;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

// Various utilities
public class Utils extends Activity {
	
	private static final String TAG = "MiscUtils";
	
	// A wrapper written by me to execute commands
	public static void exec(String command) {
		try {
			Runtime.getRuntime().exec(command);
		}
		catch (IOException e) {
			Log.e(TAG, "Error running command: " + command, e);
		}
	}
	// Copy is by Rakshi on StackOverflow (source https://stackoverflow.com/a/9293885)
	public static boolean copy(File src, File dst) {
		MessageDigest md5Src = null;
		MessageDigest md5Dst = null;
		byte[] hashIn = null;
		byte[] hashOut = null;
		InputStream in = null;
		OutputStream out = null;
		DigestInputStream din = null;
		DigestOutputStream dout = null;
		try {
			md5Src = MessageDigest.getInstance("MD5");
			md5Dst = MessageDigest.getInstance("MD5");
			in = new FileInputStream(src);
			out = new FileOutputStream(dst);
			din = new DigestInputStream(in, md5Src);
			dout = new DigestOutputStream(out, md5Dst);
			try {
				// Transfer bytes from in to out
				byte[] buf = new byte[1024];
				int len;
				while ((len = din.read(buf)) > 0) {
					dout.write(buf, 0, len);
				}
				hashIn = md5Src.digest();
				hashOut = md5Dst.digest();
			}
			finally {
				dout.close();
				out.close();
			}
		}
		catch (IOException e) {
			Log.e(TAG, "Could not copy file!", e);
		}
		catch (NoSuchAlgorithmException e) {
			Log.e(TAG, "Error getting MD5sum!", e);
		}
		finally {
			try {
				din.close();
				in.close();
			}
			catch (IOException e) {
				Log.e(TAG, "Error closing input streams!", e);
			}
		}
		boolean returnCode = false;
		if (new String(hashIn).equals(new String(hashOut))) {
			returnCode = true;
		}
		return returnCode;
	}
	// Based on code from StackOverflow by Zeeshan Ghazanfar (source https://stackoverflow.com/a/27816008)
	public static void writeFile(String filename, String content) {
		try {
			File file = new File(filename);
			FileWriter writer = new FileWriter(file);
			writer.append(content);
			writer.flush();
			writer.close();
		}
		catch (IOException e) {
			Log.e(TAG, "Could not write file!", e);
		}
	}
	// Opens a chosen URL, requires a Context to be passed because it's a static method
	public static void openURL(String URL, Context context) {
		Uri URI = Uri.parse(URL);
		Intent intent = new Intent(Intent.ACTION_VIEW, URI);
		context.startActivity(intent);
	}

	// Based off https://stackoverflow.com/a/13353219 by RajeshVijayakumar
	public static byte[] fileToBytes(File file) {

		byte[] b = new byte[(int) file.length()];
		try {
			FileInputStream fileInputStream = new FileInputStream(file);
			fileInputStream.read(b);
		}
		catch (IOException e) {
			Log.e(TAG, "Error reading file!", e);
		}
		return b;
	}

	// Based off https://stackoverflow.com/a/421696 by user49913
	public static String md5hash(File src) {

		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		}
		catch (NoSuchAlgorithmException e) {
			Log.e(TAG, "Error getting MD5 instance!", e);
		}
		m.reset();
		m.update(fileToBytes(src));
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1, digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}

	// By Martin Algesten https://stackoverflow.com/a/6694210
	public static byte[] readFromURL(String urlString) {
		byte[] out = "Error getting file from URL!".getBytes();
		try {
			out = new Scanner(new URL(urlString).openStream(), "UTF-8").useDelimiter("\\A").next().getBytes();
		}
		catch (IOException e) {
			Log.e(TAG, "Error getting file!", e);
		}
		return out;
	}

	// Runs readFromURL in a seperate thread
	public static class readFromWeb extends AsyncTask<String, Integer, byte[]> {
		@Override
		protected byte[] doInBackground(String... params) {
			return readFromURL(params[0]);
		}
	}
	
	// https://stackoverflow.com/a/36167567, pramod kumar
	public static void download(String url, String fileName, Context context) {
		DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
		request.setDescription("Downloading...");
		request.setTitle(fileName);
		request.allowScanningByMediaScanner();
		request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
		request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
		// get download service and enqueue file
		DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
		manager.enqueue(request);
	}
	
	// https://www.xkcd.com/221/
	public static int getRandomNumber()
	{
		return 4; // chosen by fair dice roll. 
		// guaranteed to be random.
	}
	
	// Legitimate PRNG (getRandomNumber is from xkcd 221 and always == 4)
	public static int legetRandomNumber (int min, int max, boolean gtZero) { //it's a pun. legit -> leGET. get it? no? ok... :(
		int gtz = (gtZero) ? 1 : 0;
		return new Random().nextInt((max - min) + gtz) + min;
		//                            ^     ^     ^     ^
		//                          start  else   must   at
		//                          with   it'd   >0?   least
		//                           max  =min to        min
		//                                max+min
	}
	// default gtZero to true if not specified
	public static int legetRandomNumber (int min, int max) {			// It's a pun, legit -> leget
		return legetRandomNumber(min, max, true);
	}
	
	public static Object[] expandArray(Object[] original) {
		original = Arrays.copyOf(original, original.length + 1);
		return original;
	}
	public static Object[][] expandArray(Object[][] original) {
		original = Arrays.copyOf(original, original.length + 1);
		original[original.length - 1] = new Object[original[original.length - 2].length];
		return original;
	}
	
	public static String timeStamp(SimpleDateFormat dateFormat) {
		return dateFormat.format(new Date());												// Variable for timestamps
	}
	
	// https://stackoverflow.com/a/5445161/
	public static String convertStreamToString(InputStream is) {
		Scanner s = new Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}
	
}
